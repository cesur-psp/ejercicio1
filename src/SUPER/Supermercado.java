package SUPER;

import java.util.concurrent.Semaphore;

public class Supermercado {
    public static void main(String[] args) {
        Semaphore semaphore=new Semaphore(2);

        Caja caja1 = new Caja("C1");
        Caja caja2 = new Caja("C2");
        Caja caja3 = new Caja("C3");

        CajaHilos cajaHilos1 = new CajaHilos(semaphore,caja1,"pablo",5);
        CajaHilos cajaHilos2 = new CajaHilos(semaphore,caja2,"lucia",5);
        CajaHilos cajaHilos3 = new CajaHilos(semaphore,caja3,"pepe",5);

        Thread hilo1 = new Thread(cajaHilos1);
        Thread hilo2 = new Thread(cajaHilos2);
        Thread hilo3 = new Thread(cajaHilos3);

        hilo1.setName(caja1.getNombre());
        hilo2.setName(caja2.getNombre());
        hilo3.setName(caja3.getNombre());

        hilo1.start();
        hilo2.start();
        hilo3.start();
    }
}
