package SUPER;

import java.util.concurrent.Semaphore;

public class CajaHilos implements Runnable {
    public Semaphore semaforo;
    public Caja caja;
    public String cliente;
    public int productos;

    public CajaHilos(Semaphore semaforo, Caja caja, String cliente, int productos) {
        this.semaforo = semaforo;
        this.caja = caja;
        this.cliente = cliente;
        this.productos = productos;
    }

    @Override
    public void run() {
        try {
            semaforo.acquire();
            caja.procesarCompra(this.cliente,this.productos);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaforo.release();
        }
    }
}
