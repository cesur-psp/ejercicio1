package SUPER;
public class Caja extends Thread {
    public String nombre;

    public Caja(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void procesarCompra(String cliente, int productos){
        System.out.println("La caja " + this.nombre + " recibe al cliente " + cliente);

        for(int i =0;i<=productos;i++){
            System.out.println("Caja " + this.nombre + " -Cliente: " + cliente + " -Producto" + i);
            try {
                int tiempoProceso=(int) (Math.random()*3);
                Thread.sleep(tiempoProceso * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("La caja " + this.nombre + " termina el proceso de compra para el cliente " + cliente);
    }

}
