package ATM;

import java.util.concurrent.Semaphore;

public class ATMMain {
    public static void main(String[] args) {
        Semaphore permiso=new Semaphore(3);
        ATM atm1 = new ATM("A1",permiso);
        ATM atm2 = new ATM("A2",permiso);
        ATM atm3 = new ATM("A3",permiso);
        ATM atm4 = new ATM("A4",permiso);
        ATM atm5 = new ATM("A5",permiso);

        atm1.start();
        atm2.start();
        atm3.start();
        atm4.start();
        atm5.start();
    }
}
