package ATM;

import java.util.concurrent.Semaphore;

class ATM extends Thread {
    public String name;
    public Semaphore semaforo;

    public ATM(String name,Semaphore semaforo) {
        this.name = name;
        this.semaforo = semaforo;
    }

    @Override
    public void run() {
        try {
            semaforo.acquire();
            int seconds = (int) (Math.random()*8+3);
            for (int i = 0; i < seconds; i++) {
                System.out.println("ATM " + name + " está en uso");
                Thread.sleep(1000); // 1 segundo
            }
        } catch (InterruptedException e) {
                e.printStackTrace();
        } finally {
            System.out.println("ATM " + name + " está libre");
            semaforo.release();
        }

    }
}